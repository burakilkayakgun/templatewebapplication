﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TemplateWebApplication.Account.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <p>
        <br />
    </p>
    <p>
        <label> Username:</label>&nbsp;&nbsp;
        <asp:TextBox ID="TxtUsername" type="text" runat="server"></asp:TextBox>
    </p>
    <p>
        <label> Password:</label>&nbsp;&nbsp;
        <asp:TextBox ID="TxtPassword" Type="password" runat="server"></asp:TextBox>
    </p>
    <p>
        <asp:Button ID="LoginButton" runat="server" Text="Login" OnClick="LoginButton_Click" />
    </p>
    <p>
        <a runat="server" href="~/Account/RequestNewPassword.aspx">Forgot Password?</a>
    </p>

</asp:Content>
