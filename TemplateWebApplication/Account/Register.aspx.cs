﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TemplateWebApplication.Models;
using System.Windows.Forms;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TemplateWebApplication.Data;

namespace TemplateWebApplication.Account
{
    public partial class Register : System.Web.UI.Page
    {
        SqlConnection connection = new SqlConnection("Data Source=EXCALIBUR;Initial Catalog=test_db;Integrated Security=True");
        DBContextEF DBContext = new DBContextEF("Data Source=EXCALIBUR;Initial Catalog=test_db;Integrated Security=True");


        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        private string FindColumnName(string tableName)
        {
            string result = "";
            switch (tableName)
            {
                case "credential":
                    result = "username";
                    break;
                case "newsletter":
                    result = "email" ;
                    break;
            }

            return result;
        }

          //
          // tablename = table name
          // element = element that want to be search in table
        private bool IsExist(string tableName, string element)
        {
            int count = 0;
            string sql = "select * from " + tableName ;
            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
            DataTable dt = new DataTable();
            connection.Open();
            adapter.Fill(dt);
            connection.Close();
            foreach (DataRow item in dt.Rows)
            {
                if (count > 0) { break; }
                if (element.Equals($"{item[FindColumnName(tableName)]}"))
                {
                    count = count + 1;
                }
            }
            if (count > 0) { return true; } else { return false; }
            
        }

        private void DisplayWelcomeMessage()
        {
            string text = "Registred succesfully";
            string title = "Welcome";
            MessageBox.Show(text, title,MessageBoxButtons.OK);
        }

        private DialogResult DisplayInvalidMessage()
        {
            string text = "That username is already taken";
            string title = "Username error";

            DialogResult result = MessageBox.Show(text, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.None);
            return result;
        }

        private void DisplayRequiredPolicyMessage()
        {
            string text = "You must be accept the privacy polcies";
            string title = "Alert";

            MessageBox.Show(text,title);
        }


        private void AddNewsletterList()
        {
            Newsletter newsletter = new Newsletter
            {
                email = TxtEmail.Text
            };

            if (!IsExist("newsletter", TxtEmail.Text))
            {
                DBContext.Newsletters.Add(newsletter);
                DBContext.SaveChanges();
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (!IsExist("credential", TxtUsername.Text))
            {
                // add to db
                if (!privacyPolicy.Checked)
                {
                    DisplayRequiredPolicyMessage();
                    return  ;
                }
                
                Credential credential = new Credential
                {
                    username = TxtUsername.Text,
                    password = TxtPassword.Text,
                    email = TxtEmail.Text
                };

                if (sendEmail.Checked)
                {
                    credential.sendEmailOption = 1;
                    AddNewsletterList();
                }

                DBContext.Credentials.Add(credential);
                DBContext.SaveChanges();
                DisplayWelcomeMessage();
            }
            else
            {
                // it means there exist a record with username as input
                // go home page or reflesh the registerpage

                DialogResult dialog = DisplayInvalidMessage();

                if (dialog == DialogResult.Retry)
                {
                    Response.Redirect("~/Account/Register.aspx");
                }
                else if (dialog == DialogResult.Cancel)
                {
                    Response.Redirect("~/");
                }

            }
        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}