﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.Windows.Forms;

namespace TemplateWebApplication.Account
{
    public partial class RequestNexPassword : System.Web.UI.Page
    {
        SqlConnection connection = new SqlConnection("Data Source=EXCALIBUR;Initial Catalog=test_db;Integrated Security=True");

        string mailFrom = "";

        private string password()
        {
            return "";
        }

        private bool IsExist(string email)
        {
            int count = 0;
            string sql = "select * from credential";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
            DataTable dt = new DataTable();
            connection.Open();
            adapter.Fill(dt);
            connection.Close();
            foreach (DataRow item in dt.Rows)
            {
                if (count > 0) { break; }
                if (email.Equals($"{item["email"]}"))
                {
                    count = count + 1;
                }
            }
            if (count > 0) { return true; } else { return false; }

        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        protected void Submit_Click(object sender, EventArgs e)
        {
            if (IsExist(TxtEmail.Text))
            {
                
                string passwordRequest = "New Password Request";
                string bodyMessage = "test";
                MailMessage mail = new MailMessage(mailFrom,TxtEmail.Text,passwordRequest,bodyMessage);
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.Credentials = new NetworkCredential(mailFrom,password());
                client.EnableSsl = true;
                client.Send(mail);
                MessageBox.Show("Mail send!","Success");
                
            }
            else
            {
                DialogResult dialog = DisplayInvalidMessage();

                if (dialog == DialogResult.Retry)
                {
                    Response.Redirect("~/Account/RequestnewPassword.aspx");
                }
                else if (dialog == DialogResult.Cancel)
                {
                    Response.Redirect("~/Account/Login.aspx");
                }
            }
        }

        private DialogResult DisplayInvalidMessage()
        {
            string text = "Mail is not registered to the system";
            string title = "Email error";

            DialogResult result = MessageBox.Show(text, title, MessageBoxButtons.RetryCancel, MessageBoxIcon.None);
            return result;
        }
    }
}