﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="TemplateWebApplication.Account.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <br />
    </div>
    <div>
        <label> Email:</label>&nbsp;&nbsp;
        <asp:TextBox ID="TxtEmail" type="text" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="rfvEmail" ControlToValidate="TxtEmail" ErrorMessage="required" Display="Dynamic" />
    </div>
    <div>
        <label> Username:</label>&nbsp;&nbsp;
        <asp:TextBox ID="TxtUsername" type="text" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="rfvName" ControlToValidate="TxtUsername" ErrorMessage="required" Display="Dynamic" />
    </div>
    <div>
        <label> Password:</label>&nbsp;&nbsp;
        <asp:TextBox ID="TxtPassword" type="password" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="rfvPassword" ControlToValidate="TxtPassword" ErrorMessage="required" Display="Dynamic" />
    </div>
    <div>
        <asp:CheckBox ID="privacyPolicy" Text="Accept the privacy policy" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged" />
    &nbsp;&nbsp;&nbsp;<a href="PrivatePolicy">Privacy Policy</a>
    </div>
    <div>
        <asp:CheckBox ID="sendEmail" Text="Subcribe to newsletter" runat="server" OnCheckedChanged="CheckBox2_CheckedChanged" />

    </div>
    <div>
        <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" />
    </div>



</asp:Content>
