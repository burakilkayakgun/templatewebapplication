﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RequestNewPassword.aspx.cs" Inherits="TemplateWebApplication.Account.RequestNexPassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <br />
    </div>
     <div>
        <label> Email:</label>&nbsp;&nbsp;
        <asp:TextBox ID="TxtEmail" type="text" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="rfvEmail" ControlToValidate="TxtEmail" ErrorMessage="required" Display="Dynamic" />
    </div>
    <div>
        <asp:Button ID="Submit" runat="server" Text="Send" OnClick="Submit_Click" />
    </div>
</asp:Content>
