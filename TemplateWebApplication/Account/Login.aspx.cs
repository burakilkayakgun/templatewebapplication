﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TemplateWebApplication.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {            
            Session["Username"] = TxtUsername.Text;
            Session["Password"] = TxtPassword.Text;

            Response.Redirect("~/Homepage.aspx");
        }
    }
}