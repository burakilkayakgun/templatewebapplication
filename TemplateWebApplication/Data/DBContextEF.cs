﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using TemplateWebApplication.Models;

namespace TemplateWebApplication.Data
{
    public class DBContextEF : System.Data.Entity.DbContext
    {
        public DBContextEF(string constr)
        {
            this.Database.Connection.ConnectionString = constr;
            this.Configuration.AutoDetectChangesEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Credential>().Property(a => a.id).IsConcurrencyToken();

            modelBuilder.Entity<Newsletter>().Property(b => b.id).IsConcurrencyToken();
            
        }

        public DbSet<Credential> Credentials { get; set; }
        public DbSet<Newsletter> Newsletters { get; set; }
    }
}